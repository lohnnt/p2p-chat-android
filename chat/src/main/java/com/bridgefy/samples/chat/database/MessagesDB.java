package com.bridgefy.samples.chat.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


@Database(entities = {MessageItem.class, PeerEntryDB.class}, version = 1, exportSchema = false)
public abstract class MessagesDB extends RoomDatabase {

    private static final String DATABASE_NAME = "message_db";

    private static MessagesDB DBINSTANCE;

    public abstract MessagesDao messagesDao();

    public abstract PeerDao peerDao();

    public static MessagesDB getDatabase(Context context) {

        if (DBINSTANCE == null) {
            synchronized (MessagesDB.class) {
                DBINSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        MessagesDB.class, DATABASE_NAME).build();
            }

        }
        return DBINSTANCE;
    }

    public static void destroyInstance() {
        DBINSTANCE = null;
    }


}
