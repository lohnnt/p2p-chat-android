package com.bridgefy.samples.chat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;

import android.os.Debug;
import android.os.Message;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Display;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridgefy.samples.chat.database.DatabaseService;
import com.bridgefy.samples.chat.database.DatabaseServiceMessagesCallback;
import com.bridgefy.samples.chat.entities.MessageEntity;
import com.bridgefy.samples.chat.entities.Peer;
import com.bridgefy.sdk.client.BFEngineProfile;
import com.bridgefy.sdk.client.Bridgefy;
import com.google.android.gms.common.internal.service.Common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bridgefy.samples.chat.MainActivity.BROADCAST_CHAT;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_IMG;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_MSG;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_NAME;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_UUID;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_DEVICE_NAME;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_DEVICE_TYPE;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_TEXT;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_USERNAME;
import static com.bridgefy.samples.chat.MainActivity.personName;
import static com.bridgefy.samples.chat.MainActivity.INTENT_CONTENT_TYPE;

import android.widget.Button;
import android.widget.Toast;




public class ChatActivity extends AppCompatActivity  {

    SharedPreferences sharedpreferences;

    private String conversationName;
    private String conversationId;

    // request codes
    private static final int MY_PERMISSION_OPEN_CAMERA = 101;
    private static final int MY_PERMISSION_READ_PHOTOS = 102;
    private static final int MY_LOCATION = 110;

    MarshmallowPermission marshmallowPermission = new MarshmallowPermission(this);
    DatabaseService db;
    public String photoFileName = "photo.jpg";
    private File file;
    public String mCurrentPhotoPath;

    @BindView(R.id.txtMessage)
    EditText txtMessage;

    MessagesRecyclerViewAdapter messagesAdapter =
            new MessagesRecyclerViewAdapter(new ArrayList<MessageEntity>());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);

        // recover our Peer object
        conversationName = getIntent().getStringExtra(INTENT_EXTRA_NAME);
        conversationId   = getIntent().getStringExtra(INTENT_EXTRA_UUID);

        // recover shared preferences on whether user wants vibrate and/or sound on message receive
        sharedpreferences= getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        // Configure the Toolbar
        Toolbar toolbar = findViewById(R.id.chat_toolbar);
        setSupportActionBar(toolbar);
        //toolbar.setTitle("YOLO");

        // Enable the Up button
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle(conversationName);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        // register the receiver to listen for local incoming messages
            // from the recieved main activity
        LocalBroadcastManager.getInstance(getBaseContext())
                .registerReceiver(new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {

                        String message_type = intent.getStringExtra(MainActivity.INTENT_CONTENT_TYPE);
                        Log.i("REV MESSAGE", message_type);
                        MessageEntity message = new MessageEntity(intent.getStringExtra(MainActivity.INTENT_EXTRA_MSG));

                        if (message_type.equals("image")) {
                            byte[] image_data = intent.getByteArrayExtra(INTENT_EXTRA_IMG);
                            message.setDirection(MessageEntity.INCOMING_IMAGE_MESSAGE);
                            message.setData(image_data);

                        }
                        else {
                            //TEXT SHOULD BE HANDLED ALREADY

                        }
/*                        //TODO, NEED TO PUT THIS INTO A DIFFERENT FUNCTION
                        MessageEntity message = new MessageEntity(intent.getStringExtra(MainActivity.INTENT_EXTRA_MSG));
                        byte[] image_data = intent.getByteArrayExtra(INTENT_EXTRA_IMG);
                        message.setData(image_data);
                        message.setUsername(intent.getStringExtra(MainActivity.INTENT_EXTRA_USERNAME));
*/
                        String username = message.getUserName();
                        message.setUsername(username);


                        messagesAdapter.addMessage(message);

                    }
                }, new IntentFilter(conversationId));

        // configure the recyclerview
        RecyclerView messagesRecyclerView = findViewById(R.id.message_list);
        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setReverseLayout(true);
        messagesRecyclerView.setLayoutManager(mLinearLayoutManager);
        messagesRecyclerView.setAdapter(messagesAdapter);

//        Button button= (Button) findViewById(R.id.trash);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                db.deleteAllMessages();
//            }
//        });


        db = new DatabaseService(this.getApplication().getApplicationContext());

        Log.i("conversation id", conversationId);
        this.loadMessages(conversationId);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @OnClick({R.id.btnSend})
    public void onMessageSend(View v) {
        // get the message and push it to the views
        String messageString = txtMessage.getText().toString();
        if (messageString.trim().length() > 0) {
            // update the views
            txtMessage.setText("");



            MessageEntity messageEntity = new MessageEntity(messageString);
            messageEntity.setDirection(MessageEntity.OUTGOING_MESSAGE);
            messageEntity.setUsername(personName);
            Long timestamp = System.currentTimeMillis()/1000; //TODO: need to fix this
            messageEntity.setTimestamp(timestamp);

            messageEntity.setConversationID(conversationId);
            Log.i("CONVERSATION", conversationId);

            messageEntity.setData(null);

            db.saveMessageToLocalDatabase(messageEntity);
            messagesAdapter.addMessage(messageEntity);



            // create a HashMap object to send
            HashMap<String, Object> content = new HashMap<>();
            content.put(PAYLOAD_TEXT, messageString);
            content.put(PAYLOAD_USERNAME, personName);
            content.put("type", "text");

            // send text message to device(s)
            if (conversationId.equals(BROADCAST_CHAT)) {
                // we put extra information in broadcast packets since they won't be bound to a session
                content.put(PAYLOAD_DEVICE_NAME, Build.MANUFACTURER + " " + Build.MODEL);
                content.put(PAYLOAD_DEVICE_TYPE, Peer.DeviceType.ANDROID.ordinal());

                com.bridgefy.sdk.client.Message.Builder builder=new com.bridgefy.sdk.client.Message.Builder();
                builder.setContent(content);
                Bridgefy.sendBroadcastMessage(builder.build(), BFEngineProfile.BFConfigProfileLongReach);
            } else {

                com.bridgefy.sdk.client.Message.Builder builder=new com.bridgefy.sdk.client.Message.Builder();
                builder.setContent(content).setReceiverId(conversationId);

                Bridgefy.sendMessage(builder.build(),
                        BFEngineProfile.BFConfigProfileLongReach);
            }
        }
    }

    private void loadMessages(String conversation) {
        db.readMessagesFromLocalDatabase(conversation, new DatabaseServiceMessagesCallback() {
            @Override
            public void messagesReadCallback(ArrayList<MessageEntity> loadedMessages) {

               for (MessageEntity message : loadedMessages) {
                   messagesAdapter.addMessage(message);
               }
            }
        });
    }


    /**
     *      RECYCLER VIEW CLASSES
     */
    class MessagesRecyclerViewAdapter
            extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.MessageViewHolder> {

        private final List<MessageEntity> messages;

        MessagesRecyclerViewAdapter(List<MessageEntity> messages) {
            this.messages = messages;
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        void addMessage(MessageEntity message) {
            messages.add(0, message);
            notifyDataSetChanged();
        }

        @Override
        public int getItemViewType(int position) {
            return messages.get(position).getDirection();
        }

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View messageView = null;

            //TODO : check if text or image
            switch (viewType) {
                case MessageEntity.INCOMING_MESSAGE:
                    messageView = LayoutInflater.from(viewGroup.getContext()).
                            inflate((R.layout.message_row_incoming), viewGroup, false);
                    break;
                case MessageEntity.OUTGOING_MESSAGE:
                    messageView = LayoutInflater.from(viewGroup.getContext()).
                            inflate((R.layout.message_row_outgoing), viewGroup, false);
                    break;
                case MessageEntity.INCOMING_IMAGE_MESSAGE:
                    messageView = LayoutInflater.from(viewGroup.getContext()).
                            inflate((R.layout.message_row_image_incoming), viewGroup, false);
                    break;
                case MessageEntity.OUTGOING_IMAGE_MESSAGE:
                    messageView = LayoutInflater.from(viewGroup.getContext()).
                            inflate((R.layout.message_row_image_outgoing), viewGroup, false);
                    break;
            }

                return new MessageViewHolder(messageView);


        }

        @Override
        public void onBindViewHolder(final MessageViewHolder messageHolder, int position) {
            messageHolder.setMessage(messages.get(position));
        }

        class MessageViewHolder extends RecyclerView.ViewHolder {
            final TextView txtMessage;
            final ImageView imgMessage;
            MessageEntity message;

            MessageViewHolder(View view) {
                super(view);
                txtMessage = view.findViewById(R.id.txtMessage);
                imgMessage = view.findViewById(R.id.txtImageMessage);
            }

            void setMessage(MessageEntity message) {
                this.message = message;
                //TODO : check if text or image then set imeage from
                Log.d("ZZZ", "setmessage debug mode");
                if (message.getDirection() == MessageEntity.INCOMING_MESSAGE &&
                        conversationId.equals(BROADCAST_CHAT)) {
                        this.txtMessage.setText(message.getUserName() + ":\n" + message.getText());
                    Log.d("ZZZ", "setMessage: is an incoming broadcast message");
                }
                else {
                    if(message.getDirection() == MessageEntity.OUTGOING_IMAGE_MESSAGE){
                        Bitmap newbitmap =  decodeFile (message.getData());
                        this.imgMessage.setImageBitmap(newbitmap);
                        Log.d("ZZZ", "setMessage: is an outgoing image message");
                    }
                    else if (message.getDirection() == MessageEntity.INCOMING_IMAGE_MESSAGE){
                        Bitmap newbitmap =  decodeFile (message.getData());
                        Log.d("ZZZ", "setMessage: is an incoming image message");
                        this.imgMessage.setImageBitmap(newbitmap);
                    }
                    else{
                        Log.d("ZZZ", "setMessage: is an outgoing message" +
                                "/ or private incoming message");
                        this.txtMessage.setText(message.getText());
                    }

                }
            }
        }

    }

    private Bitmap decodeFile(byte[] data){
        Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
        return bitmap;
    }

    public static final String EXTRA_IMAGE_DATA = "";

    // TODO: On image Click to display the full size image.
    public void onViewImageClick(View v){
       // Toast.makeText(this, "Clicked on the image", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DisplayImages.class);
        // INTENT_EXTRA_IMG data type to send
        intent.putExtra(INTENT_EXTRA_MSG, "HI");
        ImageView pic = (ImageView) v;
//        ImageView pic = (ImageView) findViewById(R.id.txtImageMessage);
//      
        BitmapDrawable drawable = (BitmapDrawable) pic.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,bos);
        byte[] bb = bos.toByteArray();

        intent.putExtra(EXTRA_IMAGE_DATA, bb);
//        intent.putExtra("position", v);
        startActivity(intent);

    }

    public void onLoadPhotoClick(View v){
        //Toast.makeText(this, "pic", Toast.LENGTH_SHORT).show();
        if(!marshmallowPermission.checkPermissionForReadfiles()){
            marshmallowPermission.requestPermissionForReadfiles();
        }
        else{
            // Create intent for picking a photo from the gallery
            Intent intent = new Intent(Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Bring up gallery to select a photo
            startActivityForResult(intent, MY_PERMISSION_READ_PHOTOS);
            //Toast.makeText(this, "You clicked gallery", Toast.LENGTH_SHORT).show();
        }
    }


//    public void onLocationClick(View v){
//        // Somehow send the location details....
//        Toast.makeText(this, "You clicked location", Toast.LENGTH_SHORT).show();
//        Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
//
//        // error handing to make sure the intent is not null
//        if (intent.resolveActivity(getPackageManager()) != null) {
//            // Start the image capture intent to take a photo
//            startActivityForResult(intent, MY_LOCATION);
//        }
//    }


    public void onCameraClick(View v){
        // Check permissions
        if (!marshmallowPermission.checkPermissionForCamera()
                || !marshmallowPermission.checkPermissionForExternalStorage()) {
            marshmallowPermission.requestPermissionForCamera();
        }  else {
            // create Intent to take a picture and return control to the calling application
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

            // set file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            photoFileName = "IMG_" + timeStamp + ".jpg";

            // Create a photo file reference
            Uri file_uri = getFileUri(photoFileName,0);

            // Add extended data to the intent
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file_uri);

            // If you call startActivityForResult() using an intent that no app can handle, your app will crash.
            // So as long as the result is not null, it's safe to use the intent.
            if (intent.resolveActivity(getPackageManager()) != null) {
                // Start the image capture intent to take photo
                startActivityForResult(intent, MY_PERMISSION_OPEN_CAMERA);
            }
        }

    }


    // Returns the Uri for a photo/media stored on disk given the fileName and type
    public Uri getFileUri(String fileName, int type) {
        Uri fileUri = null;
        try {
            String typestr = "/images/"; //default to images type
//            if (type == 1) {
//                typestr = "/videos/";
//            } else if (type != 0) {
//                typestr = "/audios/";
//            }

            // Get safe storage directory depending on type
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),
                    typestr+fileName);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.getParentFile().exists() && !mediaStorageDir.getParentFile().mkdirs()) {
                Log.d("app", "failed to create directory");
            }

            // Create the file target for the media based on filename
            file = new File(mediaStorageDir.getParentFile().getPath() + File.separator + fileName);

            // Wrap File object into a content provider, required for API >= 24
            // See https://guides.codepath.com/android/Sharing-Content-with-Intents#sharing-files-with-api-24-or-higher
            if (Build.VERSION.SDK_INT >= 24) {
                fileUri = FileProvider.getUriForFile(
                        this.getApplicationContext(),
                        "com.bridgefy.samples.chat.fileProvider", file);
            } else {
                fileUri = Uri.fromFile(mediaStorageDir);
            }
        } catch (Exception ex) {
            Log.d("getFileUri", ex.getStackTrace().toString());
        }
        return fileUri;
    }

    public byte[] bitmapConverter (Bitmap bitmap){

        int bitmapHeight = bitmap.getHeight();
        int bitmapWidth = bitmap.getWidth();

//        Display display = getWindowManager().getDefaultDisplay();
//        Point size = new Point();
//        display.getSize(size);
//        int width = size.x/2;
//        int height = size.y;

//        Log.d("ZZZ",
//                "\nwidth: " + width
//                + "\nheight: " + height
//                + "\nbitmapHeight: " + bitmapHeight
//                + "\nbitmapWidth: " + bitmapWidth);

        if(bitmapWidth > bitmapHeight){
            // display image as a landscape mode
            bitmapHeight = 640 * bitmapHeight/ bitmapWidth;
            bitmapWidth = 640;
        }
        else if (bitmapWidth < bitmapHeight){
            // display image as portrait mode
            bitmapHeight = 320 * bitmapHeight/ bitmapWidth;
            bitmapWidth = 320;
        }

        Bitmap bitmap2 = Bitmap.createScaledBitmap(bitmap,bitmapWidth,bitmapHeight,true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (Build.VERSION.SDK_INT <= 17){
            bitmap2.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        }
        else{
            bitmap2.compress(Bitmap.CompressFormat.WEBP, 20, stream);
        }
        return stream.toByteArray();
    }

    public void onImageSend (Bitmap image){

        byte[] imageData = bitmapConverter(image);

        HashMap<String, Object> content = new HashMap<>();
        //content.put(PAYLOAD_TEXT, imageData.toString());
        content.put(PAYLOAD_USERNAME, personName);
        content.put("type", "image");

        // Similar code base from sending a message.
        MessageEntity messageEntity = new MessageEntity("");
        messageEntity.setDirection(MessageEntity.OUTGOING_IMAGE_MESSAGE);
        messageEntity.setUsername(personName);
        Long timestamp = System.currentTimeMillis()/1000; //TODO: need to fix this
        messageEntity.setTimestamp(timestamp);

        messageEntity.setConversationID(conversationId);
        Log.i("CONVERSATION", conversationId);
        messageEntity.setData(imageData);


        // TODO: Work on uncommenting the database to working
        db.saveMessageToLocalDatabase(messageEntity);

        messagesAdapter.addMessage(messageEntity);

        // TODO: Work on sending and receiving the device
                // send text message to device(s)
                if (conversationId.equals(BROADCAST_CHAT)) {
                        // we put extra information in broadcast packets since they won't be bound to a session
                    content.put(PAYLOAD_DEVICE_NAME, Build.MANUFACTURER + " " + Build.MODEL);
                    content.put(PAYLOAD_DEVICE_TYPE, Peer.DeviceType.ANDROID.ordinal());

                    com.bridgefy.sdk.client.Message.Builder builder=new com.bridgefy.sdk.client.Message.Builder();
                    builder.setContent(content);
                    builder.setData(imageData);
                    Bridgefy.sendBroadcastMessage(builder.build(), BFEngineProfile.BFConfigProfileLongReach);
                    //Log.d("ZZZ", "Image sent on broadcast ");
                } else {

                    com.bridgefy.sdk.client.Message.Builder builder=new com.bridgefy.sdk.client.Message.Builder();
                    //Log.d("ZZZ", "Create builder ");
                    builder.setContent(content).setReceiverId(conversationId);
                    // Send image data
                    builder.setData(imageData);
                    //Log.d("ZZZ", "Set content on builder ");

                    Bridgefy.sendMessage(builder.build(),
                            BFEngineProfile.BFConfigProfileLongReach);
                    //Log.d("ZZZ", "Image sent on private network ");
                    }
    }

    public Bitmap SamsungPhone (Bitmap bitmap, File path){

        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (exif == null){
            return null;
        }

        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        Bitmap rotatedBitmap = null;
        switch (orientation){
            case ExifInterface.ORIENTATION_ROTATE_270:
                rotatedBitmap = rotateImage(bitmap,270);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                rotatedBitmap = rotateImage(bitmap,180);
                break;

            case ExifInterface.ORIENTATION_ROTATE_90:
                rotatedBitmap = rotateImage(bitmap,90);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
                default:
                   rotatedBitmap = bitmap;
                }
                return rotatedBitmap;

    }

    public static Bitmap rotateImage(Bitmap source, float angle){
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        Log.d("ZZZ", "rotateImage: I had rotated the image " + angle);
        return Bitmap.createBitmap(source,0,0,
                source.getWidth(), source.getHeight(),
                matrix,true);

    }

    public String getPath(Uri uri){
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        if(requestCode == MY_PERMISSION_OPEN_CAMERA){
            if(resultCode == RESULT_OK){
                Bitmap takenImage = BitmapFactory.decodeFile(file.getAbsolutePath());
                //Log.d("ZZZ", Build.MANUFACTURER.toLowerCase());
                if (Build.MANUFACTURER.toLowerCase().equals("samsung")){
                    takenImage = SamsungPhone(takenImage, file);
                }
                onImageSend(takenImage);
            }
        }

        if(requestCode == MY_PERMISSION_READ_PHOTOS){
            if(resultCode == RESULT_OK){
                // Do something with the photo
                Uri photoUri = data.getData();
                Bitmap selectedImage;
                try {
                    selectedImage = MediaStore.Images.Media.getBitmap(
                            this.getContentResolver(), photoUri);
                    if (Build.MANUFACTURER.toLowerCase().equals("samsung")){
                        //Log.d("ZZZ", photoUri.getPath() + "\n" + getPath(photoUri));
                        File testFile = new File(getPath(photoUri));
                        selectedImage = SamsungPhone(selectedImage, testFile);
                    }
                    onImageSend(selectedImage);
                }
                catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }



}
