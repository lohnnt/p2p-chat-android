package com.bridgefy.samples.chat;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bridgefy.samples.chat.entities.MessageEntity;
import com.bridgefy.samples.chat.authentication.Username;
import com.bridgefy.samples.chat.entities.MessageEntity;
import com.bridgefy.samples.chat.entities.Peer;
import com.bridgefy.samples.chat.misc.BytesUtil;
import com.bridgefy.sdk.client.Bridgefy;
import com.bridgefy.sdk.client.BridgefyClient;
import com.bridgefy.sdk.client.Device;
import com.bridgefy.sdk.client.Message;
import com.bridgefy.sdk.client.MessageListener;
import com.bridgefy.sdk.client.RegistrationListener;
import com.bridgefy.sdk.client.Session;
import com.bridgefy.sdk.client.StateListener;
import com.bridgefy.sdk.client.Config;
import com.bridgefy.sdk.client.BFEnergyProfile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bridgefy.samples.chat.database.*;

public class MainActivity extends AppCompatActivity {

    private String TAG = "MainActivity";

    static final String INTENT_EXTRA_NAME = "peerName";
    static final String INTENT_EXTRA_UUID = "peerUuid";
    static final String INTENT_EXTRA_TYPE = "deviceType";
    static final String INTENT_EXTRA_MSG  = "message";
    static final String INTENT_EXTRA_USERNAME = "r_username";
    static final String INTENT_CONTENT_TYPE = "content";
    static final String INTENT_EXTRA_IMG = "image";
    static final String BROADCAST_CHAT    = "Broadcast";
    static final String SETTINGS_PAGE     = "Settings";


    static final String PAYLOAD_DEVICE_TYPE  = "device_type";
    static final String PAYLOAD_DEVICE_NAME  = "device_name";
    static final String PAYLOAD_TEXT         = "text";
    static final String PAYLOAD_USERNAME     = "username";


    private static boolean vibrate;
    private static boolean sound;

    public static final int CHANGE_USERNAME = 101;

    DatabaseService db;
    SharedPreferences sharedpreferences;
    public static String personName;

    PeersRecyclerViewAdapter peersAdapter = new PeersRecyclerViewAdapter(new ArrayList<>());


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        sharedpreferences= getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);
        // fetch the user name
        personName = sharedpreferences.getString("username", null);


        if (personName == null){
            // pass the intent to the other screen.
            Intent intent = new Intent(this, Username.class);
            startActivityForResult(intent, CHANGE_USERNAME);
        }
        else {
            Toast.makeText(this, "Welcome " + personName + "!", Toast.LENGTH_SHORT).show();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Configure the Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        RecyclerView recyclerView = findViewById(R.id.peer_list);
        recyclerView.setAdapter(peersAdapter);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!bluetoothAdapter.isEnabled()){
            Log.d("ZZZ", "bluetooth is off");
            if(sharedpreferences.getBoolean("bluetoothNotify", true)){
                displayBluetoothNotification(bluetoothAdapter);
            }
        }


        if (isThingsDevice(this)) {
            //enabling bluetooth automatically
            //BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            bluetoothAdapter.enable();
        }

        Bridgefy.initialize(getApplicationContext(), "d188cda7-231d-4ebd-828f-d014340f5251",new RegistrationListener() {
            @Override
            public void onRegistrationSuccessful(BridgefyClient bridgefyClient) {
                // Start Bridgefy
                Config.Builder builder = new Config.Builder();
                builder.setEnergyProfile(BFEnergyProfile.HIGH_PERFORMANCE);
                builder.setEncryption(true);
                startBridgefy();
            }

            @Override
            public void onRegistrationFailed(int errorCode, String message) {

                Toast.makeText(getBaseContext(), getString(R.string.registration_error),
                        Toast.LENGTH_LONG).show();
            }
        });
        db = new DatabaseService(this.getApplication().getApplicationContext());

        this.loadKnownPeers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing())
            Bridgefy.stop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_settings:
                startActivity(new Intent(getBaseContext(), SettingsActivity.class)
                        .putExtra(INTENT_EXTRA_NAME, SETTINGS_PAGE));
                return true;
        }
        return false;
    }

    public void onSettingsButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        if (intent != null) {
            startActivity(intent);
        }
    }

    public void onChatButtonClick(View view) {
        Intent intent = new Intent(MainActivity.this, MainActivity.class);
        if (intent != null) {
            startActivity(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    /**
     *      BRIDGEFY METHODS
     */
    private void startBridgefy() {

        Bridgefy.start(messageListener, stateListener);
    }

    // TODO change for BridgefyUtils method
    public boolean isThingsDevice(Context context) {
        final PackageManager pm = context.getPackageManager();
        return pm.hasSystemFeature("android.hardware.type.embedded");
    }


    private MessageListener messageListener = new MessageListener() {
        @Override
        public void onMessageReceived(Message message) {

            // direct messages carrying a Device name represent device handshakes
            if (message.getContent().get(PAYLOAD_DEVICE_NAME) != null) {

                String sender_id = message.getSenderId();
                String username = "NOBODY";
                if ((String)message.getContent().get(PAYLOAD_USERNAME) != null) {
                    username  = (String)message.getContent().get(PAYLOAD_USERNAME);
                }

              //  String username  = (String)message.getContent().get(PAYLOAD_USERNAME);

                Peer peer = new Peer(sender_id, username);
                Log.d("PEER", peer.getUuid());
                peer.setNearby(true);
                peer.setDeviceType(extractType(message));
                peersAdapter.addPeer(peer);

                Log.i("PEER FOUND", "PEER: " + sender_id + "USERNAME: " + username);
                db.saveNewPeer(sender_id, username);
                Log.d(TAG, "Peer introduced itself: " + peer.getDeviceName());


            // any other direct message should be treated as such
            } else {


                String incomingMessage = (String) message.getContent().get("text");
                byte[] incomingData = (byte[]) message.getData();

                Log.d(TAG, "Incoming private message: " + incomingMessage);
                saveMessage(message, false);

                if (message.getContent().get("type").equals("image")) {
                    Log.i("RECIEVED IMAGE ", "image recieved");
                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                            new Intent(message.getSenderId()).putExtra(INTENT_EXTRA_IMG, incomingData).putExtra(INTENT_CONTENT_TYPE, "image"));
                }
                else {
                    Log.i("MESSGAE TO SEND ", message.toString());
                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                            new Intent(message.getSenderId()).putExtra(INTENT_EXTRA_MSG, incomingMessage).putExtra(INTENT_CONTENT_TYPE, "text"));
                }

                notifications(message);
            }


        }




        @Override
        public void onBroadcastMessageReceived(Message message) {
            // we should not expect to have connected previously to the device that originated
            // the incoming broadcast message, so device information is included in this packet
            String incomingMsg = (String) message.getContent().get(PAYLOAD_TEXT);


            String deviceName  = (String) message.getContent().get(PAYLOAD_DEVICE_NAME);
            String senderId = (String) message.getSenderId();
            String username = (String) message.getContent().get(PAYLOAD_USERNAME);
            byte[] incomingData = (byte[]) message.getData();

            Peer.DeviceType deviceType = extractType(message);

            saveMessage(message, true); //tave message to database
            notifications(message);
            if (message.getContent().get("type").equals("image")) {

                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                        new Intent(BROADCAST_CHAT)
                                .putExtra(INTENT_EXTRA_NAME, deviceName)
                                .putExtra(INTENT_EXTRA_TYPE, deviceType)
                                .putExtra(INTENT_EXTRA_IMG, incomingData)
                                .putExtra(INTENT_EXTRA_USERNAME, username)
                                .putExtra(INTENT_CONTENT_TYPE, "image"));
                Log.d(TAG, "Incoming broadcast image message: " + incomingData.toString());

            }

            else {
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(
                        new Intent(BROADCAST_CHAT)
                                .putExtra(INTENT_EXTRA_NAME, deviceName)
                                .putExtra(INTENT_EXTRA_TYPE, deviceType)
                                .putExtra(INTENT_EXTRA_MSG,  incomingMsg)
                                .putExtra(INTENT_EXTRA_USERNAME, username)
                                .putExtra(INTENT_CONTENT_TYPE, "text"));
                Log.d(TAG, "Incoming broadcast text message: " + incomingMsg);
            }

        }
    };

    private void notifications(Message message) {

        vibrate = sharedpreferences.getBoolean("vibrate", true);
        sound = sharedpreferences.getBoolean("sound", true);

        String username = (String) message.getContent().get(PAYLOAD_USERNAME);
        String content = (String) message.getContent().get(PAYLOAD_TEXT);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent intent = new Intent(MainActivity.this, MainActivity.class );
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,0,intent,0);

        if (sound) {
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), soundUri);
            r.play();
        }

        if (vibrate) {
            if (Build.VERSION.SDK_INT >= 26) {
                ((Vibrator) getSystemService(VIBRATOR_SERVICE)).vibrate(VibrationEffect.createOneShot(1000,255));
                NotificationManager notificationManager =
                        (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel channel = new NotificationChannel("default",
                        "Channel name",
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.setDescription("Channel description");
                notificationManager.createNotificationChannel(channel);

                if (message.getContent().get("type").equals("image")) {
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "default");
                    Notification messageNotification = notificationBuilder
                            .setContentTitle(username)
                            .setContentText("(Image)")
                            .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true)
                            .build();

                    notificationManager.notify(0, messageNotification);
                }
                else {
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "default");
                    Notification messageNotification = notificationBuilder
                            .setContentTitle(username)
                            .setContentText(content)
                            .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                            .setContentIntent(pendingIntent)
                            .setAutoCancel(true)
                            .build();

                    notificationManager.notify(0, messageNotification);
                }
            }
            long[] v = {500,1000};
            if (message.getContent().get("type").equals("image")) {
                Notification messageNotification = new Notification.Builder(this)
                        .setContentTitle(username)
                        .setContentText("(Image)")
                        .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, messageNotification);
            }
            else {
                Notification messageNotification = new Notification.Builder(this)
                        .setContentTitle(username)
                        .setContentText(content)
                        .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                        .setContentIntent(pendingIntent)
                        .setVibrate(v)
                        .setAutoCancel(true)
                        .build();

                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, messageNotification);
            }
        }
        else {
            if (message.getContent().get("type").equals("image")) {
                Notification messageNotification = new Notification.Builder(this)
                        .setContentTitle(username)
                        .setContentText("(Image)")
                        .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, messageNotification);
            }
            else {
                Notification messageNotification = new Notification.Builder(this)
                        .setContentTitle(username)
                        .setContentText(content)
                        .setSmallIcon(R.drawable.ic_off_the_grid_logo)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                notificationManager.notify(0, messageNotification);
            }

        }
    }

    private void saveMessage(Message message, boolean isBroadcast) {
        Log.d("**********","MESSAGE RECEIVED");
        String incomingMsg = (String) message.getContent().get(PAYLOAD_TEXT);
        String deviceName  = (String) message.getContent().get(PAYLOAD_DEVICE_NAME);
        String senderId = (String) message.getSenderId();
        String username = (String) message.getContent().get(PAYLOAD_USERNAME);


        MessageEntity messageEntity = new MessageEntity(incomingMsg);

        messageEntity.setUsername(username);

        Log.d("ZZZ", "saveMessage: Message recieve for type: " + message.getContent().get("type"));
        if(message.getContent().get("type").equals("image")){
            messageEntity.setDirection(MessageEntity.INCOMING_IMAGE_MESSAGE);
            messageEntity.setData(message.getData());
//            Log.i("**** EX", "successfull");
//            Log.i("BITMAP", "bitmap converted");
//            Log.d("ZZZ", "saveMessage: Set to incoming IMAGE message");
        }
        else{
            messageEntity.setDirection(MessageEntity.INCOMING_MESSAGE);
//            Log.d("ZZZ", "saveMessage: Set to incoming message");
        }




        Long timestamp = System.currentTimeMillis()/1000; //TODO: need to fix this
        messageEntity.setTimestamp(timestamp);

        Log.i("TIMESTAMP", timestamp.toString());



        if (isBroadcast) {
            messageEntity.setConversationID(BROADCAST_CHAT);
        }
        else {
            Log.i("SENDER ID ", senderId);
            messageEntity.setConversationID(senderId);
        }

        db.saveMessageToLocalDatabase(messageEntity);

    }


    private Peer.DeviceType extractType(Message message) {
        int eventOrdinal;
        Object eventObj = message.getContent().get(PAYLOAD_DEVICE_TYPE);
        if (eventObj instanceof Double) {
            eventOrdinal = ((Double) eventObj).intValue();
        } else {
            eventOrdinal = (Integer) eventObj;
        }
        return Peer.DeviceType.values()[eventOrdinal];
    }



    StateListener stateListener = new StateListener() {

        @Override
        public void onDeviceConnected(final Device device, Session session) {
            Log.i(TAG, "onDeviceConnected: " + device.getUserId());
            // send our information to the Device
            HashMap<String, Object> map = new HashMap<>();

            map.put(PAYLOAD_DEVICE_NAME, Build.MANUFACTURER + " " + Build.MODEL);
            map.put(PAYLOAD_DEVICE_TYPE, Peer.DeviceType.ANDROID.ordinal());
            map.put(PAYLOAD_USERNAME, personName); //sends username on handshake

            device.sendMessage(map);

            //TODO needs to check the known peers.
            //add it to the list of known peeris if havent seen before

        }

        @Override
        public void onDeviceLost(Device device) {
            Log.w(TAG, "onDeviceLost: " + device.getUserId());
            peersAdapter.removePeer(device);
        }

        @Override
        public void onStartError(String message, int errorCode) {
            Log.e(TAG, "onStartError: " + message);

            if (errorCode == StateListener.INSUFFICIENT_PERMISSIONS) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
        }
    };



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // Start Bridgefy
            startBridgefy();

        } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, "Location permissions needed to start peers discovery.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }





    private void loadKnownPeers() {
        db.loadAllKnownPeers(new DatabaseServicePeersCallback() {
            @Override
            public void knownPeersCallback(ArrayList<Peer> AllPeers) {

                for (Peer peer : AllPeers ) {
                    peersAdapter.addPeer(peer);
                }
            }
        });
    }





    /**
     *      RECYCLER VIEW CLASSES
     */
    class PeersRecyclerViewAdapter
            extends RecyclerView.Adapter<PeersRecyclerViewAdapter.PeerViewHolder> {

        private final List<Peer> peers;

        PeersRecyclerViewAdapter(List<Peer> peers) {
            this.peers = peers;
        }

        @Override
        public int getItemCount() {
            return peers.size();
        }

        void addPeer(Peer peer) {
            int position = getPeerPosition(peer.getUuid());
            if (position > -1) {
                peers.set(position, peer);
                notifyItemChanged(position);
            } else {
                peers.add(peer);
                notifyItemInserted(peers.size() - 1);
            }
        }

        void removePeer(Device lostPeer) {
            int position = getPeerPosition(lostPeer.getUserId());
            if (position > -1) {
                Peer peer = peers.get(position);
                peer.setNearby(false);
                peers.set(position, peer);
                notifyItemChanged(position);
            }
        }

        private int getPeerPosition(String peerId) {
            for (int i = 0; i < peers.size(); i++) {
                if (peers.get(i).getUuid().equals(peerId))
                    return i;
            }
            return -1;
        }

        @Override
        public PeerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.peer_row, parent, false);
            return new PeerViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final PeerViewHolder peerHolder, int position) {
            peerHolder.setPeer(peers.get(position));
        }

        class PeerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            final TextView mContentView;
            Peer peer;

            PeerViewHolder(View view) {
                super(view);
                mContentView = view.findViewById(R.id.peerName);
                view.setOnClickListener(this);
            }

            void setPeer(Peer peer) {
                this.peer = peer;

                switch (peer.getDeviceType()) {
                    case ANDROID:
                        this.mContentView.setText(peer.getDeviceName());
                        break;

                    case IPHONE:
                        this.mContentView.setText(peer.getDeviceName() + " (iPhone)");
                        break;
                }

                if (peer.isNearby()) {
                    this.mContentView.setTextColor(Color.BLACK);
                } else {
                    this.mContentView.setTextColor(Color.GRAY);
                }
            }

            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), ChatActivity.class)
                        .putExtra(INTENT_EXTRA_NAME, peer.getDeviceName())
                        .putExtra(INTENT_EXTRA_UUID, peer.getUuid()));

            }
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == CHANGE_USERNAME){
            if(resultCode == RESULT_OK){
                // update the sharedpreference to hold the user's username.
                SharedPreferences.Editor editor = sharedpreferences.edit();
                String name =  data.getExtras().getString("username");
                editor.putString("username", name);
                editor.commit();
                personName = name;
                //Toast.makeText(this, "Welcome " + personName + "!", Toast.LENGTH_SHORT).show();
                Log.d("ZZZ", "onActivityResult: " + name);
            }
        }

    }

    public void displayBluetoothNotification (BluetoothAdapter bluetoothAdapter) {
        // just display the option to turn on bluetooth or not....
        Log.i("MainActivity", "displayBluetoothNotification: ");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Turn on bluetooth")
                .setMessage("OFF THE GRID uses bluetooth to find nearby peers. Do you want to enable Bluetooth?")
                .setPositiveButton("YES, ENABLE BLUETOOTH", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Turn on bluetooth
                        bluetoothAdapter.enable();
                    }
                })
                .setNegativeButton("NEVER ASK ME AGAIN", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Do a sharedpreference on it setting it to not show notificaiton ever
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putBoolean("bluetoothNotify", false);
                        editor.commit();
                    }
                })
                .setNeutralButton("NO, THANKS", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Nothing happens
                    }
                });

        builder.create().show();
    }}
