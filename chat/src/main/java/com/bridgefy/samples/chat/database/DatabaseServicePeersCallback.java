package com.bridgefy.samples.chat.database;


import com.bridgefy.samples.chat.entities.*;

import java.util.ArrayList;
import java.util.List;



public interface DatabaseServicePeersCallback {

    void knownPeersCallback(ArrayList<Peer> peers);
}
