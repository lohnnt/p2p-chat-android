package com.bridgefy.samples.chat.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MessagesDao {


    @Query("SELECT * FROM messages")
    List<MessageItem> listAll();

    //gets the saved messages by conversation id
    @Query("SELECT * FROM messages WHERE conv_id = :conv_id ORDER BY timestamp")
    List<MessageItem> loadConversation(String conv_id);

    @Insert
    void insert(MessageItem messageItem);

    @Insert
    void insertAll(MessageItem... messageItems);

    @Query("DELETE FROM messages")
    void deleteAll();

}
