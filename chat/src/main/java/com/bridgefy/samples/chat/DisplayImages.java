package com.bridgefy.samples.chat;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Display;
import android.widget.ImageView;

public class DisplayImages extends AppCompatActivity {

    ImageView image;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayimage);
//        // Get intent that started this activity

        Intent intent = getIntent();
        byte[] img =  intent.getByteArrayExtra(ChatActivity.EXTRA_IMAGE_DATA);

        image = (ImageView) findViewById(R.id.pictureView);
        Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);

        int bitmapHeight = bitmap.getHeight();
        int bitmapWidth = bitmap.getWidth();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        bitmapHeight = width * bitmapHeight/ bitmapWidth;
        bitmapWidth = width;


        bitmap = Bitmap.createScaledBitmap(bitmap,bitmapWidth,bitmapHeight,true);


        image.setImageBitmap(bitmap);

        // Configure the Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Enable the Up button
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle("Image");
            ab.setDisplayHomeAsUpEnabled(true);
        }



    }



}