package com.bridgefy.samples.chat.authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bridgefy.samples.chat.MainActivity;
import com.bridgefy.samples.chat.R;

public class Username extends AppCompatActivity {

    EditText username;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_username);
        Intent intent = getIntent();

        // Configure the Toolbar
        Toolbar toolbar = findViewById(R.id.username_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("OFF THE GRID");

    }


    public void onSubmitClick (View v){
        Intent intent = new Intent();
        // Add the username to the intent
        username = (EditText)findViewById(R.id.etUsername);
        String input =  username.getText().toString();
        Log.d("ZZZ", "onSubmitClick:  Put into: " +input);
        intent.putExtra("username", input);
        setResult(RESULT_OK, intent);
        finish();
    }

}
