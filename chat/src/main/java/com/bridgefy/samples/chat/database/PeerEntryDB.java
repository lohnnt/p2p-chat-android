package com.bridgefy.samples.chat.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.bridgefy.samples.chat.entities.MessageEntity;




@Entity(tableName = "Peers")
public class PeerEntryDB {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="uid")
    private int uid; //unique id for the message

    @NonNull
    @ColumnInfo(name = "device_id")
    private String deviceID; //the user of the message

    @NonNull
    @ColumnInfo(name="username")
    private String username;



    //constructor
    public PeerEntryDB(String deviceID, String username) {
        this.deviceID = deviceID;
        this.username = username;
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid ) {
        this.uid = uid;
    }

    public String getDeviceID() { return deviceID; }

    public void setDeviceID(String deviceID) { this.deviceID = deviceID; }

    public  String getUsername() {return this.username; }

    public  void setUsername(String username) {this.username = username; }








}
