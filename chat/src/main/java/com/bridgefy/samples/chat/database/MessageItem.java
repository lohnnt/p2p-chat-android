package com.bridgefy.samples.chat.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.bridgefy.samples.chat.entities.MessageEntity;




@Entity(tableName = "Messages")
public class MessageItem {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="uid")
    private int uid; //unique id for the message

    @NonNull
    @ColumnInfo(name = "conv_id")
    private String conversationID; //the user of the message

    @NonNull
    @ColumnInfo(name="sender")
    private String sender;

    @ColumnInfo(name = "timestamp")
    private Long timestamp;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "direction")
    private int direction;

    @ColumnInfo(name = "image")
    private byte[] data;


    //constructor
    public MessageItem(String conversationID, Long timestamp, String message, int direction, String sender, byte[] data) {
        this.conversationID = conversationID;
        this.timestamp = timestamp;
        this.message = message;
        this.direction = direction;
        this.sender = sender;
        this.data = data;
    }

    //i think this refers to a bridgefy message type, will be changed
    //TODO: will only save to broadcast messages
    public MessageItem(MessageEntity message) {

        this.conversationID = message.getConversationID();
        this.timestamp = message.getTimestamp();
        this.direction = getDirection();
        this.message = message.getText();
        this.sender = message.getUserName();
        this.data = message.getData();
    }


    public int getUid() {
        return uid;
    }

    public void setUid(int uid ) {
        this.uid = uid;
    }


    public String getConversationID() {
        return  conversationID;
    }

    public void setConversationID(String conversation_id) {
        this.conversationID = conversation_id;
    }


    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }


    public String getMessage() {
        return  message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public int getDirection() {
        return  direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String  getSender() { return sender; }

    public void setSender(String username) { this.sender = sender; }

    public byte[] getData() {return data;}

    public void setData() {this.data = data;}






}
