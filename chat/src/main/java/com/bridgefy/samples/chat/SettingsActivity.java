package com.bridgefy.samples.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.util.Log;

import com.bridgefy.samples.chat.authentication.Username;
import com.bridgefy.samples.chat.database.DatabaseService;

import com.bridgefy.samples.chat.entities.MessageEntity;
import com.bridgefy.samples.chat.entities.Peer;
import com.bridgefy.sdk.client.BFEngineProfile;
import com.bridgefy.sdk.client.Bridgefy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import android.widget.Toast;

import static com.bridgefy.samples.chat.MainActivity.BROADCAST_CHAT;
import static com.bridgefy.samples.chat.MainActivity.CHANGE_USERNAME;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_NAME;
import static com.bridgefy.samples.chat.MainActivity.INTENT_EXTRA_UUID;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_DEVICE_NAME;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_DEVICE_TYPE;
import static com.bridgefy.samples.chat.MainActivity.PAYLOAD_TEXT;
import static com.bridgefy.samples.chat.MainActivity.personName;

import android.widget.Button;


public class SettingsActivity extends AppCompatActivity {

    private String screenName;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        Intent intent = getIntent();

        sharedpreferences= getApplicationContext().getSharedPreferences("MyPref", Context.MODE_PRIVATE);

        screenName = getIntent().getStringExtra(INTENT_EXTRA_NAME);

        // Configure the Toolbar
        Toolbar toolbar = findViewById(R.id.settings_toolbar);
        setSupportActionBar(toolbar);
        // Enable the Up button
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setTitle("SETTINGS");
            ab.setDisplayHomeAsUpEnabled(true);
        }

        Switch switch_sound = (Switch) findViewById(R.id.setting_sound);
        Switch switch_vibrate = (Switch) findViewById(R.id.setting_vibrate);

        //final SharedPreferences.Editor editor = sharedpreferences.edit();
        switch_sound.setChecked(sharedpreferences.getBoolean("sound", true));
        switch_vibrate.setChecked(sharedpreferences.getBoolean("vibrate", true));

        switch_sound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("sound", isChecked);
                editor.apply();
            }
        });

        switch_vibrate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                final SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("vibrate", isChecked);
                editor.apply();
            }
        });
    }

    public void onChangeUsernameClick(View v){
        Intent intent1 = new Intent(this, Username.class);
        startActivityForResult(intent1, CHANGE_USERNAME);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == CHANGE_USERNAME){
            if(resultCode == RESULT_OK){
                // update the shared preference to hold the user's username.
                SharedPreferences.Editor editor = sharedpreferences.edit();
                String name =  data.getExtras().getString("username");
                editor.putString("username", name);
                editor.commit();
                personName = name;
                Toast.makeText(this, "New name: " + personName , Toast.LENGTH_SHORT).show();
                Log.d("ZZZ", "onActivityResult: " + name);
            }
        }

    }
}
