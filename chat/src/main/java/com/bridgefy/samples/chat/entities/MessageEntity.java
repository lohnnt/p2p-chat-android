package com.bridgefy.samples.chat.entities;

import com.bridgefy.samples.chat.database.MessageItem;
import com.google.gson.Gson;

/**
 * @author dekaru on 5/9/17.
 *
 * class for displaying messages
 */

public class MessageEntity {

    public final static int INCOMING_MESSAGE = 0;
    public final static int OUTGOING_MESSAGE = 1;
    public final static int INCOMING_IMAGE_MESSAGE = 2;
    public final static int OUTGOING_IMAGE_MESSAGE = 3;

    private int    direction;
    private String username;
    private String text;
    private long timestamp;
    private String conversationID;
    private String filePath;
    private byte[] data;

    public MessageEntity(String text) {
        this.text = text;
    }

    public enum MessageType { text, image };

    private MessageType _messageType = MessageType.text;

    /*
    * convert database version MessageItem to current MessageEntity
    * */
    public MessageEntity(MessageItem messageItem) {
        this.direction = messageItem.getDirection();
        this.text = messageItem.getMessage();
        this.username = messageItem.getSender();
        this.conversationID = messageItem.getConversationID();
        this.data = messageItem.getData();
    }

    //TODO: add image

    public void setMessageType(String word){
        if (word.equals("text")){
            _messageType = MessageType.text;
        }
        else if (word.equals("image")){
            _messageType = MessageType.image;
        }
    }

    public MessageType getMessageType() { return _messageType; }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getUserName() {
        return username;
    }

    public String getText() {
        return text;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setTimestamp(long timestamp) {this.timestamp = timestamp; }

    public long getTimestamp() { return timestamp; }

    public String getConversationID() { return conversationID; }

    public void setConversationID(String conversationID) { this.conversationID = conversationID; }

    public void setFilePath(String filePath) {this.filePath = filePath;}

    public String getFilePath(){return filePath;}

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }




    public static MessageEntity create(String json) {
        return new Gson().fromJson(json, MessageEntity.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
