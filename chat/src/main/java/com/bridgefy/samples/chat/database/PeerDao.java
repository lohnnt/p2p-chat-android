package com.bridgefy.samples.chat.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PeerDao {

    //returns all the peers we have seen previously
    @Query("SELECT * FROM peers")
    List<PeerEntryDB> listAll();

    @Query("SELECT * FROM peers WHERE device_id = :deviceID ")
    List<PeerEntryDB> findPeerByDeviceID(String deviceID);

    @Query("SELECT * FROM peers WHERE username = :username ")
    List<PeerEntryDB> findPeerByUsername(String username);

    @Query("DELETE FROM peers")
    void deleteAll();

    //TODO: put in an update query here



    @Insert
    void insert(PeerEntryDB peerEntry);

    @Insert
    void insertAll(PeerEntryDB... peerEntryDBs);


}
