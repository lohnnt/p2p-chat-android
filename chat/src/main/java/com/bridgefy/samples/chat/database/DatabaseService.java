package com.bridgefy.samples.chat.database;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.bridgefy.samples.chat.entities.*;

import java.util.ArrayList;
import java.util.List;

public class DatabaseService {

    MessagesDB db;
    MessagesDao messagesDao;
    PeerDao peerDao;


    public DatabaseService(Context context) {
        db = MessagesDB.getDatabase(context);
        messagesDao = db.messagesDao();
        peerDao = db.peerDao();

    }


    public void deleteAllMessages() {


        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected  Void doInBackground(Void...voids) {
                    messagesDao.deleteAll();
                    return  null;
                }

            }.execute();
        }
        catch (Exception ex ) {
            Log.e("exception", ex.getStackTrace().toString());
        }
    }


    /*
     * saves message to local database
     *   input is a message entity type
     *   converts it to a MessageItem type, which can be saved into a database
     *   saves the message asynchonously
     *   */

    public void saveMessageToLocalDatabase(MessageEntity newMessage) {



        try {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected  Void doInBackground(Void...voids) {
                        //convert entity type to database type
                       // MessageItem messageItem = new MessageItem(newMessage);

                        MessageItem messageItem = new MessageItem(newMessage.getConversationID(), newMessage.getTimestamp(),
                                    newMessage.getText(), newMessage.getDirection(), newMessage.getUserName(), newMessage.getData());

                        Log.i("MESSAGE TRYING TO SAVE", messageItem.getSender());
                        messagesDao.insert(messageItem);


                    return  null;
                }

            }.execute();
        }
        catch (Exception ex ) {
            Log.e("exception", ex.getStackTrace().toString());
        }
    }




    public void readMessagesFromLocalDatabase(String conversationId, DatabaseServiceMessagesCallback callback) {
        new ReadMessagesTask(callback, conversationId).execute();
    }

    private class ReadMessagesTask extends AsyncTask<Void, Void, Void> {

        final DatabaseServiceMessagesCallback callback;

        private String conversationID;

        private ArrayList<MessageEntity> result = new ArrayList<MessageEntity>();

        ReadMessagesTask(DatabaseServiceMessagesCallback callback, String converationID) {
            this.callback = callback;
            this.conversationID = converationID;
        }

        @Override
        protected  Void doInBackground(Void...voids) {
            List<MessageItem> msgsFromDB = messagesDao.loadConversation(conversationID);

            //converts message item from the db to the displayable enitity type
            ArrayList<MessageEntity> messagesToDisplay = new ArrayList<MessageEntity>();
            Log.i("READ FROM DATABASE", "DOING IN BACKGROUND");


            if (msgsFromDB != null & msgsFromDB.size() > 0) {
                for (MessageItem messageItem : msgsFromDB) {
                    Log.i("RETRIEVED MESSAGE", messageItem.getMessage() +" for conversation " + messageItem.getConversationID());

                    MessageEntity currentMessage = new MessageEntity(messageItem);
                    messagesToDisplay.add(currentMessage);


                }
                result = messagesToDisplay;
                Log.i("READ FROM DATABASE", result.toString());
              //  callback.messagesReadCallback(messagesToDisplay);

            }

            return  null;
        }

        @Override
        protected void onPostExecute(Void v) {
            Log.i("READ FROM DATABASE", "callback working");
            if (result.size() > 0) {


            } else {
                Log.i("no reuslts", "none");
            }


            callback.messagesReadCallback(result);
        }
    }





    public void saveNewPeer(String deviceId, String username) {


        try {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected  Void doInBackground(Void...voids) {
                    PeerEntryDB newPeer = new PeerEntryDB(deviceId, username);

                    List<PeerEntryDB> result = peerDao.findPeerByDeviceID(deviceId);
                    if  (result.size() == 0) {
                        Log.i("NO PEER", "PEER");
                    }
                    else {
                        Log.i("PEER SAVED", result.toString());
                    }

                    peerDao.insert(newPeer);
                    Log.i("INSERTING PEER", "ID: " + newPeer.getDeviceID() + " with username of " + newPeer.getUsername());
                    return null;
                }

            }.execute();
        }
        catch (Exception ex ) {
            Log.e("exception", ex.getStackTrace().toString());
        }
    }

    public void loadAllKnownPeers(DatabaseServicePeersCallback callback) {
        new LoadAllKnownPeers(callback).execute();
    }




    private class LoadAllKnownPeers extends AsyncTask<Void, Void, Void> {

        final DatabaseServicePeersCallback callback;

        private ArrayList<Peer> results = new ArrayList<Peer>();

        LoadAllKnownPeers(DatabaseServicePeersCallback callback) {
            this.callback = callback;
        }

        @Override
        protected  Void doInBackground(Void...voids) {

                List<PeerEntryDB> KnownPeers = peerDao.listAll();
            if (KnownPeers != null & KnownPeers.size() > 0) {
                for (PeerEntryDB knownPeer : KnownPeers) {

                    Peer current_peer = new Peer(knownPeer.getDeviceID(), knownPeer.getUsername());
                    current_peer.setDeviceType(Peer.DeviceType.ANDROID);
                    results.add(current_peer);

                }
                return null;
            }

            return  null;
        }

        @Override
        protected void onPostExecute(Void v) {

            if (results.size() > 0) {

            } else {
                Log.i("no results", "none");
            }


            callback.knownPeersCallback(results);
        }
    }




    public void saveNewPeer(Peer newPeer) {

    }





}
