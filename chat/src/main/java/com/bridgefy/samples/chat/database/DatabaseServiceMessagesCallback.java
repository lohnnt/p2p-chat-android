package com.bridgefy.samples.chat.database;

import com.bridgefy.samples.chat.entities.*;

import java.util.ArrayList;
import java.util.List;


/*interface
*    that handles the callback for returning messages from file
*   as read from the local database
* */
public interface DatabaseServiceMessagesCallback {

    void messagesReadCallback(ArrayList<MessageEntity> messages);

}


